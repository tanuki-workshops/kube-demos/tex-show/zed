const assert = require('assert')
const { message, randomInteger } = require("./main.testable.js")


describe('test message function', () => {
  it('should be true', () => {
    assert.equal(message(),"👋 🌍")
  })
})

// Silly test 😜
describe('test randomInteger function', () => {
  it('should be true', () => {
    assert.notEqual(randomInteger(1,20),99)
  })
})

describe('42 == 42', () => {
  it('should be true', () => {
    assert.equal(42,42)
  })
})

describe('0 == 0', () => {
	it('should be true', () => {
	  assert.equal(0,0)
	})
})
